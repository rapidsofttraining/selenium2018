package testng;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class LoginAndSearchTests {

	WebDriver driver;
	Commonutility commonutil = new Commonutility();

	@BeforeTest
	public void beforeTest() {
		System.out.println("in @beforeTest");
		// commonutil.startBrowser(driver);
		System.setProperty("webdriver.chrome.driver", "C:\\ws\\coreselenium\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void loginTest() throws InterruptedException {
		System.out.println("in @test loginTest");
		driver.get("http://newtours.demoaut.com/");
		
		// string comp
		Assert.assertEquals(driver.getTitle(), "Welcome: Mercury Tours");
		// true condition
		
		boolean checkbox = true;
		Assert.assertTrue(checkbox, "The checkbox is not checked");
		
		boolean loginbuttORpresentornot = verifyloginButton();
		Assert.assertTrue(loginbuttORpresentornot, "The login button is NOT present");
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement username = driver.findElement(By.xpath("//input[@name='userName']"));
		username.sendKeys("demo");
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("demo");
		WebElement login = driver.findElement(By.name("login"));
		login.click();
		Thread.sleep(5000);

	}

	@Test
	public void searchTest() throws InterruptedException {
		System.out.println("test 22222222222222222222222222222222     ");

		List<WebElement> roundTripRadios = driver.findElements(By.name("tripType"));
		// roundTripRadios.get(0).click();
		roundTripRadios.get(1).click();
		roundTripRadios.get(1).getText();
		Thread.sleep(3000);

		WebElement departLoc = driver.findElement(By.name("fromPort"));

		Select dropdown = new Select(departLoc);

		List<WebElement> departlocations = dropdown.getOptions();

		for (WebElement location : departlocations) {
			System.out.println("printing depart locations - " + location.getText());
		}

		dropdown.selectByVisibleText("New York");

		WebElement continuebutton = driver.findElement(By.name("findFlights"));

		// WebElement continuebutton =
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("findFlights")));
		// WebElement continuebutton1 =
		// wait.until(ExpectedConditions.elementToBeClickable(By.name("findFlights")));
		System.out.println("" + driver.getTitle());

		continuebutton.click();

		Thread.sleep(5000);
	}

	@AfterTest
	public void afterTest() {
		System.out.println("in @afterTest");
		driver.quit();

	}
	
	public boolean verifyloginButton()
	{
		
		return false;
		
	}

}
