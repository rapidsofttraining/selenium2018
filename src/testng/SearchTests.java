package testng;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

public class SearchTests {

	WebDriver driver= null;

	public void startBrowser(WebDriver driver) {
		// System.setProperty("webdriver.chrome.driver","C:\\Deepa\\selenium\\chromedriver_win32\\chromedriver.exe");
		// driver = new ChromeDriver();

	}

	@BeforeSuite
	public void Beforelogin() {
		System.setProperty("webdriver.chrome.driver", "C:\\Deepa\\selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		System.out.println("in @Beforetest loginTest");
		driver.get("http://newtours.demoaut.com/");

		//WebDriverWait wait = new WebDriverWait(driver, 20);
	}

	@Test
	public void loginUser() throws InterruptedException {
		System.out.println("in @Test-logUser");
		// WebElement username = driver.findElement(By.name("userName"));
		WebElement username = driver.findElement(By.xpath("//input[@name='userName']"));
		// WebElement username =
		// driver.findElement(By.xpath("//tbody/tr[4]/td/table/tbody/tr[2]/td[2]/input"));

		username.sendKeys("demo");

		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("demo");

		WebElement login = driver.findElement(By.name("login"));
		login.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Thread.sleep(15000);
        System.out.println("Title = " + driver.getTitle());
		List<WebElement> roundTripRadios = driver.findElements(By.name("tripType"));
		// roundTripRadios.get(0).click();
		Thread.sleep(3000);
		System.out.println ("-----------------------------------");
		System.out.println("size = " +roundTripRadios.size());
		for(WebElement radio: roundTripRadios)
		{
			
			System.out.println(radio.getText() );
		}
		
		
//		roundTripRadios.get(1).click();
//		roundTripRadios.get(1).getText();
//		roundTripRadios.get(1).isSelected();

		Select passenger = new Select(driver.findElement(By.name("passCount")));
		passenger.selectByIndex(1);
		Select departing = new Select(driver.findElement(By.name("fromPort")));
		departing.selectByVisibleText("London");
		Select onMonth = new Select(driver.findElement(By.name("fromMonth")));
		onMonth.selectByVisibleText("April");
		Select onDate = new Select(driver.findElement(By.name("fromDay")));
		onDate.selectByIndex(25);
		Select arriving = new Select(driver.findElement(By.name("toPort")));
		arriving.selectByValue("Seattle");
		Select toMonth = new Select(driver.findElement(By.name("toMonth")));
		toMonth.selectByVisibleText("August");
		Select fromDate = new Select(driver.findElement(By.name("toDay")));
		fromDate.selectByIndex(28);
		List<WebElement> serviceClass = driver.findElements(By.name("servClass"));
		serviceClass.get(1).click();

		serviceClass.get(1).getText();
		driver.findElement(By.name("findFlights")).click();
		

	}

	@Test
	public void secondTest()
	{
		System.out.println("Second test");
		if(driver == null)
		{
			System.out.println("driver is null");
		}
		else
		{
			System.out.println("driver is NOT null");
		}
		
		
	}
	@Ignore @Test
	public void flightSearch() throws InterruptedException {

		System.out.println("in @Test-MainSearch");
		Thread.sleep(15000);
        System.out.println("Title = " + driver.getTitle());
		List<WebElement> roundTripRadios = driver.findElements(By.name("tripType"));
		// roundTripRadios.get(0).click();
		Thread.sleep(3000);
		System.out.println ("-----------------------------------");
		System.out.println("size = " +roundTripRadios.size());
		for(WebElement radio: roundTripRadios)
		{
			
			System.out.println(radio.getText() );
		}
		
		
//		roundTripRadios.get(1).click();
//		roundTripRadios.get(1).getText();
//		roundTripRadios.get(1).isSelected();

		Select passenger = new Select(driver.findElement(By.name("passCount")));
		passenger.selectByIndex(1);
		Select departing = new Select(driver.findElement(By.name("fromPort")));
		departing.selectByVisibleText("London");
		Select onMonth = new Select(driver.findElement(By.name("fromMonth")));
		onMonth.selectByVisibleText("April");
		Select onDate = new Select(driver.findElement(By.name("fromDay")));
		onDate.selectByIndex(25);
		Select arriving = new Select(driver.findElement(By.name("toPort")));
		arriving.selectByValue("Seattle");
		Select toMonth = new Select(driver.findElement(By.name("toMonth")));
		toMonth.selectByVisibleText("August");
		Select fromDate = new Select(driver.findElement(By.name("toDay")));
		fromDate.selectByIndex(28);
		List<WebElement> serviceClass = driver.findElements(By.name("servClass"));
		serviceClass.get(1).click();

		serviceClass.get(1).getText();
		driver.findElement(By.name("findFlights")).click();

	}

	@AfterSuite
	public void afterTest() {
		System.out.println("in @afterTest");
		driver.quit();

	}

}
