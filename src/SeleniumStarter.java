import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumStarter {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\ws\\coreselenium\\Drivers\\chromedriver.exe");
		  WebDriver driver = new ChromeDriver();
		  driver.get("http://www.rapidsoftcorp.com");
		  System.out.println(driver.getCurrentUrl());
		  System.out.println(driver.getTitle());
		  WebElement aboutelement = driver.findElement(By.linkText("ABOUT"));
		  aboutelement.click();
		  Thread.sleep(6000);
		  System.out.println(driver.getTitle());
		  if (driver.getTitle().equals("About � RapidSoft Corp"))
		  {
			  // assertion
			  System.out.println("You are on About page.");
		  }
		  driver.quit();
	}

}
