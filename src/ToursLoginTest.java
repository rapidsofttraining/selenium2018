import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ToursLoginTest {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "C:\\ws\\coreselenium\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");

		// driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);

		// WebElement username = driver.findElement(By.name("userName"));
		WebElement username = driver.findElement(By.xpath("//input[@name='userName']"));
		// WebElement username =
		// driver.findElement(By.xpath("//tbody/tr[4]/td/table/tbody/tr[2]/td[2]/input"));

		username.sendKeys("demo");

		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("demo");

		WebElement login = driver.findElement(By.name("login"));
		login.click();
		Thread.sleep(5000);
		//
		// List<WebElement> roundTripRadios = driver.findElements(By.name("tripType"));
		// roundTripRadios.get(0).click();

		List<WebElement> roundTripRadios = driver.findElements(By.name("tripType"));
		// roundTripRadios.get(0).click();
		roundTripRadios.get(1).click();
		roundTripRadios.get(1).getText();
		Thread.sleep(3000);

		WebElement departLoc = driver.findElement(By.name("fromPort"));

		Select dropdown = new Select(departLoc);

		List<WebElement> departlocations = dropdown.getOptions();

		for (WebElement location : departlocations) {
			System.out.println("printing depart locations - " + location.getText());
		}

		dropdown.selectByVisibleText("New York");

		// WebElement continuebutton = driver.findElement(By.name("findFlights"));

		WebElement continuebutton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("findFlights")));
		// WebElement continuebutton1 =
		// wait.until(ExpectedConditions.elementToBeClickable(By.name("findFlights")));
		continuebutton.click();

		Thread.sleep(5000);
		System.out.println(driver.getTitle());
		driver.quit();

	}

}
